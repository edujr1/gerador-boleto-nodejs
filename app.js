'use strict'

const express = require('express');
const app = express();
const path = require('path');
const favicon = require('serve-favicon');
const compression = require('compression');
var bodyParser = require('body-parser');
const router = express.Router();

/* Adiciona o Icon com o serve-favicon */
app.use(favicon(require('path').join(__dirname,'/src/imgs/favicon.ico')));

/* Compacta todos os dados HTTP */
app.use(compression());

app.use(bodyParser.json()); 

//Rotas
const index = require('./src/routes/index');
const boleto = require('./src/routes/boleto');

app.use('/', index);
app.use('/api/boleto', boleto);

module.exports = app;
