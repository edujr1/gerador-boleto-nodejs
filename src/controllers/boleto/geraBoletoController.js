'use strict'
const config = require("../../configs/boletoConfig");
const throwCustomError = require("../../helpers/customException");
const Boleto = require('node-boleto').Boleto;

exports.geraBoleto = (req, res, next) => {
    criaBoleto(req.body, res)
};


function criaBoleto(jsonInfos, res) {
    var boleto = new Boleto({
        'banco': jsonInfos.banco,
        'data_emissao': new Date(jsonInfos.data_emissao),
        'data_vencimento': new Date(jsonInfos.data_vencimento),
        'valor': jsonInfos.valor,
        'nosso_numero': jsonInfos.nosso_numero,
        'numero_documento': jsonInfos.numero_documento,
        'cedente': jsonInfos.cedente,
        'cedente_cnpj': jsonInfos.cedente_cnpj,
        'agencia': jsonInfos.agencia,
        'codigo_cedente': jsonInfos.codigo_cedente,
        'carteira': jsonInfos.carteira
    });
    console.log("Linha digitável: " + boleto['linha_digitavel'])
    boleto.renderHTML(function (html) {
        res.send(html);
    });
}


/* 
    JSON BOLETO EXEMPLO
        {
        'banco': "santander", // nome do banco dentro da pasta 'banks'
        'data_emissao': "05/02/2018", // mm/dd/aaaa,
        'data_vencimento': "05/06/2018", // mm/dd/aaaa
        'valor': 1500, // R$ 15,00 (valor em centavos)
        'nosso_numero': "1234567",
        'numero_documento': "123123",
        'cedente': "Pagar.me Pagamentos S/A",
        'cedente_cnpj': "18727053000174", // sem pontos e traços
        'agencia': "3978",
        'codigo_cedente': "6404154", // PSK (código da carteira)
        'carteira': "102"
      }
*/