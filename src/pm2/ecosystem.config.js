module.exports = {
  apps: [

    {
      name: 'gera-boletos',
      script: require('path').join(__dirname, '../bin/server.js'),
      instances: "max",
      exec_mode: "cluster",
      windowsHide: true,
      env: {
        "NODE_ENV": "production",
        "PORT": "3000"
      }
    }


  ]
};
