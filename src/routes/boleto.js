'use strict'

const express = require('express');
const router = express.Router();
const geraBoletoController = require('../controllers/boleto/geraBoletoController')


// http://localhost:3000/api/boleto/geraBoleto
router.post('/geraBoleto', geraBoletoController.geraBoleto);


module.exports = router;