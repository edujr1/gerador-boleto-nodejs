'use strict'

module.exports.errorToObject = function (mensagem, codigo) {

    console.log(mensagem)

    var jsonError = {
        consultaComSucesso: false,
        mensagemErro: (mensagem ? mensagem : 'Mensagem não capturada'),
        codigoErro: (codigo ? codigo : 'Codigo não capturado')
    };

    return jsonError;
};