# Requisitos mínimos
- Instalar ultima versão estável do NodeJS https://nodejs.org/en/download/

# Deploy Desenvolvimento
- Caso seja a primeira execução do projeto em sua maquina, execute o seguinte comando dentro da pasta do projeto para a instalação das dependencias
```
npm install
```
- Depois para iniciar o projeto execute entro da pasta do projeto
```
npm run server
```

# Deploy Produção
- Caso seja o primeiro deploy de uma aplicação com PM2 no servidor, execute o seguinte comando para instalar globalmente o PM2
```
npm install -g pm2
```

- Agora vamos incluir o projeto na lista de execuções do PM2
```
pm2 start src/pm2/ecosystem.config.js
```

- Em seguida podemos verificar que o projeto esta rodando atravez do comando
```
pm2 list 

ou

pm2 status
```
## Finalização da implantação em Windows
- Para finalizar e sempre ter o projeto rodando no PM2 automaticamente ao reiniciar a maquina, deve-se incluir o arquivo `/src/pm2/run-pm2.bat` no agendador de tarefas, com o gatilho para executar sempre que a maquina iniciar e fizer login

## Finalização da implantação em Linux
- Para finalizar e salvar o projeto no PM2 para que sempre que a maquina for reiniciada, o PM2 execute automaticamente este projeto devemos executar os seguintes comandos

- Caso seja o primeiro projeto no PM2, execute este comando para criar um arquivo de inicialização automatica

```
pm2 startup
```

- Caso não seja o primeiro projeto rode apenas esse comando para salvar o estado atual do PM2 no arquivo de inicialização
```
pm2 save
```

# Comandos PM2
- Visualização dos logs da aplicação
```
pm2 logs
```

- Visualizar a lista de processos do PM2
```
pm2 list 

ou

pm2 status
```

- Matar o PM2 e seus serviços
```
pm2 kill
```

- Remover um ou todos os processos
```
pm2 delete (ids: 0,1,2)

ou

pm2 delete all
```

- Iniciar um projeto
```
pm2 start ecosystem.config.js

ou

pm2 start (NomeJsMainDoProjeto)
```

- Pausar um ou todos os projetos 
```
pm2 stop (ids: 0,1,2)

ou

pm2 stop all
```

- Recarregar um ou todos os projetos ao atualizar seu codigo 
```
pm2 reload (ids: 0,1,2)

ou

pm2 reload all
```

- Abrir terminal de monitoramento dos serviços
```
pm2 monit
```

- Criar serviço de inicialização automatica do PM2
```
pm2 startup
``` 

- Remover serviço de inicialização automatica do PM2
```
pm2 unstartup
``` 

- Salvar estado do PM2 no 'startup' para quando a maquina reiniciar, ele conseguir subir todos os projetos que estavam executando da ultima vez (Sempre executar este comando ao adicionar, remover, pausar e startar um projeto no PM2)
```
pm2 save
``` 